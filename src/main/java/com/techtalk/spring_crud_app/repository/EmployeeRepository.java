package com.techtalk.spring_crud_app.repository;

import com.techtalk.spring_crud_app.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Integer>{

}
