package com.techtalk.spring_crud_app.services;

import com.techtalk.spring_crud_app.model.Employee;
import com.techtalk.spring_crud_app.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EmployeeService {


    @Autowired

    private EmployeeRepository employeeRepository;


    public Employee addEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }


    public Employee getEmployeeByID(int emp_id) {
        return employeeRepository.findById(emp_id).orElse(null);
    }

    public Employee updateEmployee(Employee employee) {
        Employee existingEMP = employeeRepository.findById(employee.getEmp_id()).orElse(null);

        if (existingEMP == null) {
            return employeeRepository.save(employee);
        } else {
            employeeRepository.deleteById(existingEMP.getEmp_id());
            employeeRepository.save(employee);
        }
        return employee;
    }


    public Map getByBloodGroup() {
        List<Employee> bloodGroupList = new ArrayList<>();
        bloodGroupList = employeeRepository.findAll();

        Map<String, List<Employee>> bloodGroupMap = bloodGroupList.stream().collect(Collectors.groupingBy(em -> em.getEmp_bloodgroup()));
        return bloodGroupMap;
    }


    public List<Employee> underpaid() {
        List<Employee> underPaidList = new ArrayList<>();
        underPaidList = employeeRepository.findAll();

        List<Employee> underpaidList = new ArrayList<>();
        underpaidList = underPaidList.stream().filter(up -> up.getEmp_ctc() / up.getEmp_experience() <= 1).collect(Collectors.toList());

        return underpaidList;
    }

    public List<Employee> overpaid() {
        List<Employee> overPaidList = new ArrayList<>();
        overPaidList = employeeRepository.findAll();

        List<Employee> overpaidList = new ArrayList<>();
        overpaidList = overPaidList.stream().filter(up -> up.getEmp_ctc() / up.getEmp_experience() > 1).collect(Collectors.toList());

        return overpaidList;

    }
}
