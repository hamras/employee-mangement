package com.techtalk.spring_crud_app.controller;

import com.techtalk.spring_crud_app.model.Employee;
import com.techtalk.spring_crud_app.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/employees")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
        Employee e = employeeService.addEmployee(employee);
        return ResponseEntity.of(Optional.of(e));
    }


    @GetMapping("/employees/{emp_id}")
    public ResponseEntity<Employee> getEmployeeByID(@PathVariable int emp_id) {

        Employee employee = employeeService.getEmployeeByID(emp_id);

        if (employee == null) {
            throw new RecordNotFoundException("Invalid employee id : " + emp_id);
        }
        return ResponseEntity.of(Optional.of(employee));
    }

    @PutMapping("/employees/{emp_id}")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
        employeeService.updateEmployee(employee);
        return ResponseEntity.ok().body(employee);
    }


    @GetMapping("/employees/bloodgroup")
    public ResponseEntity<Map> getByBloodGroup() {

        Map bloodgroup = employeeService.getByBloodGroup();
        if (bloodgroup.size() <= 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.of(Optional.of(bloodgroup));
    }

    @GetMapping("/employees/underpaid")
    public ResponseEntity<List<Employee>> underpaid() {
        List<Employee> underpaid = employeeService.underpaid();
        if (underpaid.size() <= 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.of(Optional.of(underpaid));
    }

    @GetMapping("/employees/overpaid")
    public ResponseEntity<List<Employee>> overpaid() {
        List<Employee> overpaid = employeeService.overpaid();
        if (overpaid.size() <= 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.of(Optional.of(overpaid));
    }
}

